import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppComponent } from './app.component';
import { AppRoutingModule } from './app-routing.module';
import { HttpClientModule } from '@angular/common/http';
import { StocksModule } from './stocks/stocks.module';

@NgModule({
  declarations: [AppComponent],
  imports: [BrowserModule, HttpClientModule, StocksModule, AppRoutingModule],
  bootstrap: [AppComponent]
})
export class AppModule {}
