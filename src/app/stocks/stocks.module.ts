import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { StocksComponent } from './stocks.component';
import { StocksService } from './stocks.service';

@NgModule({
  imports: [CommonModule],
  declarations: [StocksComponent],
  providers: [StocksService]
})
export class StocksModule {}
