import { Component, OnInit, OnDestroy } from '@angular/core';
import { Subscription, timer } from 'rxjs';
import { StocksService } from './stocks.service';
import { HttpErrorResponse } from '@angular/common/http';

@Component({
  selector: 'app-stocks',
  templateUrl: './stocks.component.html',
  styleUrls: ['./stocks.component.scss']
})
export class StocksComponent implements OnInit, OnDestroy {
  // companies has to be public not private so the html can see it
  gainers: any[];
  losers: any[];
  private timer$: Subscription;

  // Inject Stocks Service in Component's constructor
  constructor(private stocksService: StocksService) {}

  ngOnInit() {
    this.gainers = [];
    this.losers = [];
    // Start timer observable that gets the companies data each 10 seconds to keep it real-time
    this.startTimer();
  }

  private startTimer() {
    this.timer$ = timer(0, 10000).subscribe(this.getStockData.bind(this));
  }

  private getStockData() {
    this.stocksService.getStockGainers().subscribe(
      (res: any[]) => {
        this.gainers = res;
      },
      (err: HttpErrorResponse) => {
        alert(err);
      }
    );

    this.stocksService.getStockLosers().subscribe(
      (res: any[]) => {
        this.losers = res;
      },
      (err: HttpErrorResponse) => {
        alert(err);
      }
    );
  }

  ngOnDestroy() {
    // Stop the timer on component destroy to prevent memory leaks
    this.timer$.unsubscribe();
  }
}
