import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { environment } from '../../environments/environment';

@Injectable()
export class StocksService {
  // Inject Angular's HttpClient
  constructor(private httpClient: HttpClient) {}

  getStockGainers() {
    return this.httpClient.get(
      `${environment.apiUrl}/stock/market/list/gainers`
    );
  }

  getStockLosers() {
    return this.httpClient.get(
      `${environment.apiUrl}/stock/market/list/losers`
    );
  }
}
