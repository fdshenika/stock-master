# Name

Shenika Fernando

# DummydataStocks

This project was generated with [Angular CLI](https://github.com/angular/angular-cli) version 6.2.3.

## Time Spent:

1 Hour

## How to run:

*   `npm install`
*   `ng serve`

incase if this doesn't work due to your local path issue , run like this:-

*   `npm run start:dev`

*   url : `http://localhost:4200/`
